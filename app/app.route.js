'use strict';

angular.module('zorkansound', ['ngRoute', 'home', 'artist', 'events', 'geo', 'event', 'album', 'charts', 'tag'])
	.config(function($routeProvider, $locationProvider) {


		$routeProvider
			.when('/', {
				templateUrl: 'scripts/home/homeView.html',
				controller: 'homeCtrl'
			})
			.when('/events', {
				templateUrl: 'scripts/events/eventsView.html',
				controller: 'eventsCtrl'
			})
			.when('/artists/:artistName', {
				templateUrl: 'scripts/artist/artistView.html',
				controller: 'artistCtrl'
			})
			.when('/geo', {
				templateUrl: 'scripts/geo/geoView.html',
				controller: 'geoCtrl'
			})
			.when('/events/:eventId', {
				templateUrl: 'scripts/event/eventView.html',
				controller: 'eventCtrl'

			})
			.when('/charts', {
				templateUrl: 'scripts/charts/chartsView.html',
				controller: 'chartsCtrl'

			})
			.when('/album/:artistName/:albumName', {
				templateUrl: 'scripts/album/albumView.html',
				controller: 'albumCtrl'

			})
			.when('/tag/:tagName', {
				templateUrl: 'scripts/tag/tagView.html',
				controller: 'tagCtrl'

			})
			.when('/account', {
				templateUrl: 'scripts/account/accountView.html',

			})
			.otherwise({
				templateUrl: 'scripts/error/errorView.html',

			});
	});