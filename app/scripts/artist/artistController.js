'use strict';

angular.module('artist', ['services'])
	.controller('artistCtrl', ['$scope', '$routeParams', '$q', 'api', function($scope, $routeParams, $q, api) {

		$scope.artist = $routeParams.artistName;
		$scope.displayDate = 16;
		$q.all([
			api.getArtistInfo($scope.artist, 'info'),
			api.getArtistInfo($scope.artist, 'topalbums'),
			api.getArtistInfo($scope.artist, 'events')

		]).then(
			function(data) {
				$scope.artistInfo = data[0].data.artist;
				$scope.topAlbums = data[1].data.topalbums.album;
				$scope.events = data[2].data.events.event;

				console.log($scope.events);
			},

			function(reason) {
				console.log(reason);
			});
	}]);