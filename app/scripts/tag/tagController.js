'use strict';

angular.module('tag', ['services'])
	.controller('tagCtrl', ['$routeParams', '$scope', '$q', 'api', function($routeParams, $scope, $q, api) {
		$scope.tagName = $routeParams.tagName;
		$scope.tagDisplay = 5;
		console.log($scope.tagName);
		$q.all([
			api.getTagInfo($scope.tagName, 'topartists'),
			api.getTagInfo($scope.tagName, 'toptracks'),
			api.getTagInfo($scope.tagName, 'similar'),

		]).then(
			function(data) {
				$scope.topArtists = data[0].data.topartists.artist;
				$scope.topTracks = data[1].data.toptracks.track;
				$scope.similar = data[2].data.similartags.tag;
			},

			function(reason) {
				console.log(reason);
			});
	}]);