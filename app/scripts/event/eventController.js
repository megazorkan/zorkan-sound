'use strict';

angular.module('event', ['services'])
	.controller('eventCtrl', ['$routeParams', '$scope', '$q', 'api', function($routeParams, $scope, $q, api) {

		$scope.eventId = $routeParams.eventId;
		$scope.seeMoreCounter = 5;
		$scope.hasVenueImage = false;
		$scope.hasConcertImage = false;
		$scope.displayDate = 22;

		$scope.seeMore = function() {
			$scope.seeMoreCounter += 10;
		};

		$scope.seeMoreLeft = function() {
			if ($scope.seeMoreCounter >= $scope.numberOfArtists) {
				return true;
			}
			return false;
		};

		api.getEventInfo($scope.eventId, 'info').then(
			function(eventInfo) {
				$scope.eventInfo = eventInfo.data.event;
				$scope.numberOfArtists = eventInfo.data.event.artists.artist.length;

				($scope.eventInfo.venue.image[3]['#text'] !== '') ? $scope.hasVenueImage = true: $scope.hasVenueImage = false;

				($scope.eventInfo.image[3]['#text'] !== '') ? $scope.hasConcertImage = true: $scope.hasConcertImage = false;



				var geoLat = $scope.eventInfo.venue.location['geo:point']['geo:lat'];
				var geoLong = $scope.eventInfo.venue.location['geo:point']['geo:long'];

				//set up google map
				/*
				if (geoLat !== '' && geoLong !== '') {
					var mapCanvas = document.getElementById('map-canvas');
					var mapOptions = {
						center: new google.maps.LatLng(geoLat, geoLong),
						zoom: 16,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					var map = new google.maps.Map(mapCanvas, mapOptions);

					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(geoLat, geoLong),
						map: map,
						title: $scope.eventInfo.venue.name
					});
				}
				*/


			},
			function() {
				console.log('error');
			})

	}]);