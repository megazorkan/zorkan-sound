'use strict';

angular.module('services')
	.filter('capitalize', function() {
		return function(word) {
			return word[0].toUpperCase() + word.substr(1);
		};
	})
	.filter('editDate', function() {
		return function(date, position) {
			return (date.substr(0, position));
		};
	})
	.filter('secondsToMinutes', function() {
		return function(secondsString) {

			var minutes = Math.floor(Number(secondsString / 60));
			var minutesString = String(minutes < 10 ? '0' + minutes : minutes);

			var seconds = Number(secondsString) - Number(minutes) * 60;
			secondsString = String(seconds < 10 ? '0' + seconds : seconds);

			return (minutesString + ':' + secondsString);
		};
	})
	.filter('commaOutNumber', function() {
		return function(number) {

			var reverse = number.split('').reverse();
			var solution = [];
			for (var i = 0, j = number.length; i < j; i++) {
				if (i % 3 === 0 && i !== 0) {
					solution.push(',');
				}
				solution.push(reverse[i]);
			}
			return solution.reverse().join('');
		};
	})
	.filter('removeHTMLTags', function() {
		return function(html) {
			var div = document.createElement('div');
			div.innerHTML = html;
			return div.innerText;
		};
	});