'use strict';

angular.module('services', [])
	.constant('baseUrl', 'http://ws.audioscrobbler.com/2.0/?')
	.constant('apiKey', '6249bed18e6f7991ffb0546caa9dad64')
	.constant('dataFormat', '&format=json')
	.factory('api', function($http, apiKey, baseUrl) {
		return {

			/***************************************
				=CHARTS 
			***************************************/

			getCharts: function(category) {
				var address = baseUrl + 'method=chart.get' + category + '&api_key=' + apiKey + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			},

			/****************************************
				=ARTISTS 
			****************************************/

			getArtistInfo: function(artist, info) {
				var address = baseUrl + 'method=artist.get' + info + '&artist=' + artist + '&api_key=' + apiKey + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			},

			/*****************************************
				EVENT
			*****************************************/

			getEventInfo: function(eventId, info) {
				var address = baseUrl + 'method=event.get' + info + '&event=' + eventId + '&api_key=' + apiKey + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			},

			/*****************************************
				GEO
			*****************************************/

			getGeoInfo: function(country, info, attribute) {
				var address = baseUrl + 'method=geo.get' + info + '&' + attribute + '=' + country + '&api_key=' + apiKey + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			},

			/*****************************************
				ALBUM
			*****************************************/

			getAlbumInfo: function(artistName, albumName, info) {
				var address = baseUrl + 'method=album.get' + info + '&api_key=' + apiKey + '&artist=' + artistName + '&album=' + albumName + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			},

			/*****************************************
				TAG
			*****************************************/

			getTagInfo: function(tagName, info) {
				var address = baseUrl + 'method=tag.get' + info + '&tag=' + tagName + '&api_key=' + apiKey + '&format=json';
				return $http.get(address).success(function(data) {
					return data;
				});
			}

		};

	});