'use strict';

angular.module('geo', ['services'])
	.controller('geoCtrl', function($scope, $q, api) {

		$scope.country = 'germany';
		$scope.displayDate = 16;
		$scope.$watch('country', function() {
			$scope.getData();
		});

		$scope.countries = ['germany', 'spain', 'italy', 'bulgaria', 'croatia', 'serbia',
			'austria', 'slovenia', 'macedonia', 'russia', 'switzerland', 'sweden',
			'finland', 'norway', 'hungary', 'netherlands', 'belgium', 'france'
		];

		$scope.getData = function() {
			$q.all([
				api.getGeoInfo($scope.country, 'toptracks', 'country'),
				api.getGeoInfo($scope.country, 'topartists', 'country'),
				api.getGeoInfo($scope.country, 'events', 'location')
			]).then(
				function(data) {
					console.log(data);
					$scope.topTracks = data[0].data.tracks.track;
					$scope.topArtists = data[1].data.topartists.artist;
					$scope.events = data[2].data.events.event;
				},

				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData();



	});