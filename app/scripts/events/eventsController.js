'use strict';

angular.module('events', ['services', 'ui.router'])
	.controller('eventsCtrl', ['$rootScope', '$state', '$scope', 'api', function($rootScope, $state, $scope, api) {

		$scope.country = 'germany';
		$scope.displayDate = 16;
		$scope.$watch('country', function() {
			$scope.getData($scope.country);
		});


		$scope.getCountryInput = function(country) {
			$scope.country = country;
		};

		//todo - najbolje bi bilo jednom napraviti da pronade 5,6 najblizih gradova oadbranom gradu
		// 		 jebes ove dzrave hardkodirane
		$scope.countries = ['germany', 'spain', 'italy', 'bulgaria', 'croatia', 'serbia',
			'austria', 'slovenia', 'macedonia', 'russia', 'switzerland', 'sweden',
			'finland', 'norway', 'hungary', 'netherlands', 'belgium', 'france'
		];

		$scope.getData = function(country) {

			api.getGeoInfo(country, 'events', 'location')
				.then(
					function(data) {
						$scope.events = data.data.events.event;

					},
					function() {

					});
		};

		$scope.getData($scope.country);
	}]);