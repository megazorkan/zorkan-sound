'use strict';

angular.module('home', ['services'])
	.controller('homeCtrl', ['$scope', '$location', '$q', 'api', function($scope, $location, $q, api) {

		$scope.displayNumber = 4;
		$scope.displayNumberTracks = 10;
		$scope.displayDate = 16;
		$scope.country = 'new york';


		$scope.$watch('country', function() {
			api.getGeoInfo($scope.country, 'events', 'location').then(
				function(events) {
					$scope.geoEvents = events.data.events.event;
				});
		});

		$scope.findArtist = function(artistName) {
			$location.url('/artists/' + artistName);
		};

		$scope.checkAttribute = function(attribute) {
			if (attribute === undefined) {
				return false;
			}
			return true;
		};

		$scope.getData = function(country) {
			$q.all([


				api.getCharts('toptracks'),
				api.getCharts('topartists'),
				api.getCharts('toptags'),
				api.getGeoInfo(country, 'events', 'location')

			]).then(
				function(data) {
					$scope.topTracks = data[0].data.tracks.track;
					$scope.topArtists = data[1].data.artists.artist;
					$scope.topTags = data[2].data.tags.tag;
					$scope.geoEvents = data[3].data.events.event;
				},

				function(reason) {
					//$location.url('/error');
					console.log(reason);

				});
		};

		$scope.getData($scope.country);

	}]);