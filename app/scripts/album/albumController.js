'use strict';

angular.module('album', ['services'])
	.controller('albumCtrl', ['$routeParams', '$scope', '$q', 'api', function($routeParams, $scope, $q, api) {
		$scope.artistName = $routeParams.artistName;
		$scope.albumName = $routeParams.albumName;

		api.getAlbumInfo($scope.artistName, $scope.albumName, 'info').then(
			function(albumInfo) {
				$scope.albumInfo = albumInfo.data.album;
				console.log($scope.albumInfo);
			},
			function() {
				console.log('error');
			});

	}]);