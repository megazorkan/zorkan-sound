'use strict';

angular.module('charts', ['services'])
	.controller('chartsCtrl', ['$routeParams', '$scope', '$q', 'api', function($routeParams, $scope, $q, api) {

		$scope.displayData = 5;
		$scope.getData = function() {
			$q.all([
				api.getCharts('topartists'),
				api.getCharts('hypedartists'),
				api.getCharts('toptracks'),
				api.getCharts('hypedtracks'),
				api.getCharts('lovedtracks')

			]).then(
				function(data) {
					$scope.topArtists = data[0].data.artists.artist;
					$scope.hypedArtists = data[1].data.artists.artist;
					$scope.topTracks = data[2].data.tracks.track;
					$scope.hypedTracks = data[3].data.tracks.track;
					$scope.lovedTracks = data[4].data.tracks.track;

					console.log(data);
				},

				function() {
					console.log('error');
				});
		};

		$scope.getData();

	}]);